const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

// Policy helper function
var generatePolicy = (principalId, effect, resource,contexto) => {
  var authResponse = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    const statementOne = {};
    statementOne.Action = 'execute-api:Invoke';
    statementOne.Effect = effect;
    statementOne.Resource = resource;
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  authResponse.context = contexto;
  console.log(authResponse);
  console.log(authResponse.policyDocument)
    console.log(authResponse.policyDocument.Statement[0])

  return authResponse;
};

module.exports.auth = (event, context, callback) => {
  const token = event.headers.Authorization;
  const secret = event.stageVariables.argonSecret;
  if (!token)
    return callback(null, 'Unauthorized');
    
  var arn=event.methodArn;
  var resource = arn.substr(arn.lastIndexOf(":")+1).split("/");
  var metodo = resource[2];
  var uri = resource[3].slice(0, -1);
    console.log(arn);

  // AuthN
  var mytoken = token.replace('Bearer ','');
  jwt.verify(mytoken, secret, (err, decoded) => {
    if (err){
      return callback(null, generatePolicy(null, 'Deny', arn ,err));
    }
  var userId = decoded._id;
  // AuthZ  

    //el user es admin, puede hacer lo que quiera
    return callback(null, generatePolicy(userId, 'Allow', arn ,decoded));
  });

};